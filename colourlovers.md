#Colourlovers api endpoints

##Patterns

http://www.colourlovers.com/api/patterns
http://www.colourlovers.com/api/patterns/new
http://www.colourlovers.com/api/patterns/top
http://www.colourlovers.com/api/patterns/random

###Api endpoints

/patterns/new
/patterns/top
/patterns/random

###Parametros

orderCol = [dateCreated, score, name, numVotes, numViews]
sortBy = [ASC, DESC]
format = [json, xml]
numResults = [1...100]

##Datos a almacenar

Patterns:
	id
	title
	dateCreated
	imageUrl
	apiUrl
	authors : [url, url]

##Wallpapers api endpoints

Resolution format:

1900x1080x1

(width) x (height) x (scaling factor)

GET : http://wallpapers.mikephoto.es/:id/:resolution/[format]

## Other pattern pages

http://subtlepatterns.com/thumbnail-view/