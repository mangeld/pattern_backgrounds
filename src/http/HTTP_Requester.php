<?php
namespace backgrounder\http;

/**
 * 
 * 
 * TODO: Implement POST, PUT, DELETE, headers!
 **/
class HTTP_Requester
{
	private $url;
	private $headers = array();
	private $params = array();
	/** @var Method */
	private $method;
	private $curl;

	public function __construct($url = null)
	{
		$this->url = $url;
		$this->method = new Method();
		$this->curl = curl_init();
		$this->set_url($url);
	}

	public function download_file(\backgrounder\objects\File $destination, $url = null)
	{
		if($url !== null) $this->url = $url;
		$local_file = fopen($destination->get_path(), 'w');
		$res = fopen($this->url, 'r');
		$data = stream_get_contents($res);
		fwrite($local_file, $data);
		fclose($local_file);
		fclose($res);
	}

	public function set_variable($name, $value)
	{
		$this->params[$name] = $value;
	}

	public function set_header($name, $value)
	{
		$this->headers[$name] = $value;
	}

	public function set_url($url)
	{
		curl_setopt($this->curl, CURLOPT_URL, $url);
		$this->url = $url;
	}

	public function execute()
	{
		if($this->curl === false) $this->curl = curl_init();
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);

		if($this->method->verb() === Method::GET)
		{
			curl_setopt($this->curl, CURLOPT_URL, $this->bake_get_url());
			$data = curl_exec($this->curl);
			curl_close($this->curl);
			$this->curl = false;
			return $data;
		}
	}

	public function flush_variables()
	{
		$this->params = array();
	}

	private function bake_get_url()
	{
		$baked = $this->url.'&';

		foreach ($this->params as $name => $value)
		{
			$baked .= urlencode($name).'='.urlencode($value).'&';
		}
		$baked = rtrim($baked, '&');

		return $baked;
	}
}