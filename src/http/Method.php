<?php
namespace backgrounder\http;

class Method
{
	const GET = 0;
	const POST = 1;
	const PUT = 2;
	const DELETE = 3;
	const OPTIONS = 4;
	const HEAD = 5;
	const TRACE = 6;
	const CONNECT = 7;

	private $verb;

	public function __construct($verb = Method::GET)
	{
		$this->verb = $verb;
	}

	public function set_verb($verb)
	{
		$this->verb = $verb;
	}

	public function verb()
	{
		return $this->verb;
	}
}