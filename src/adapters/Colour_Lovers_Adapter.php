<?php

namespace backgrounder\adapters;

use backgrounder\http\HTTP_Requester;

class Colour_Lovers_Adapter
{
	private $requester;
	private $NEW_PATTERNS_URL = 'http://www.colourlovers.com/api/patterns/new';

	public function __construct(HTTP_Requester $requester)
	{
		$this->requester = $requester;
		$this->init_request_variables();
	}

	private function init_request_variables()
	{
		$this->requester->set_variable('format', 'json');
	}

	public function get_new_patterns_url()
	{
		$this->requester->set_url($this->NEW_PATTERNS_URL);
		$this->requester->set_variable('numResults', 100);
		$data = json_decode($this->requester->execute(), true);

		$out = array();
		$c = 0;

		foreach($data as $key => $value)
		{
			$out[$c] = $value['imageUrl'];
			$c = $c + 1;
		}

		return $out;
	}

}