<?php
namespace backgrounder\objects;

class File
{
	private $path;

	public function __construct($path)
	{
		$this->path = $path;
	}

	public function is_file()
	{
		return is_file($this->path);
	}

	public function is_folder()
	{
		return is_dir($this->path);
	}

	public function get_path()
	{
		return $this->path;
	}

	public function get_name()
	{
		return basename($this->path);
	}
}