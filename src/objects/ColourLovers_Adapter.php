<?php

namespace backgrounder\objects;
use backgrounder\http\HTTP_Requester;

class ColourLovers_Adapter
{
	private $requester;
	const BASE_URL = 'http://www.colourlovers.com/api/';
	const NEW_PATTERNS_URL = 'http://www.colourlovers.com/api/patterns/new';

	private $variables = array(
		'format' => 'json',
		);

	public function __construct()
	{
		$this->requester = new HTTP_Requester();
		$this->init_http();
	}

	private function init_http()
	{
		foreach ($this->variables as $key => $value)
		{
			$this->requester->set_variable($key, $value);
		}
	}

	public function get_new_patterns($n = 10)
	{
		$this->requester->set_url(NEW_PATTERNS_URL);
		$this->requester->set_variable('numResults', $n);
	}

	public function get_pattern($id)
	{

	}
}