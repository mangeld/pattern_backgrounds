<?php

namespace backgrounder\objects;

class Size
{
	/**@var integer*/
	private $width;
	/**@var integer*/
	private $height;

	public function __construct($width = 100, $height = 100)
	{
		$this->width = $width;
		$this->height = $height;
	}

	public function get_height()
	{
		return $this->height;
	}

	public function get_width()
	{
		return $this->width;
	}

	public function get_size()
	{
		return $this;
	}

	public function set_height($height)
	{
		$this->height = $height;
	}

	public function set_width($width)
	{
		$this->width = $width;
	}
}