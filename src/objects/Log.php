<?php

namespace backgrounder\objects;

class Log
{
	private static $instance;
	private $messages = array();

	private function __construct() {}

	public function __destruct()
	{
		$archivo = fopen($_SERVER['PWD'].'/log.txt', 'a');
		foreach ($this->messages as $key => $value){ fwrite($archivo, $value."\n"); }
		fclose($archivo);
	}

	static public function get_instance()
	{
		if( self::$instance == null ){ self::$instance = new Log(); }
		return self::$instance;
	}

	public function i($message) { array_push($this->messages, $message); }

	public function get_messages() { return $this->messages; }
}