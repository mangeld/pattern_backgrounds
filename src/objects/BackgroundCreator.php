<?php

namespace backgrounder\objects;

class BackgroundCreator
{
	public function __construct()
	{

	}

	public function createBackground(Image $pattern, Resolution $resolution)
	{
		$background = Image::from_resolution($resolution);
		$width = $background->get_resolution()->get_width();
		$height = $background->get_resolution()->get_height();
		imagesettile($background->get_resource(), $pattern->get_resource());
		imagefilledrectangle($background->get_resource(), 0, 0, $width, $height, IMG_COLOR_TILED);
		imagepng($background->get_resource(), '/home/mike/test.png');
	}

}