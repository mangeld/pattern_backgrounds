<?php

namespace backgrounder\objects;

class Image
{
	/** @var Size*/
	protected $size;
	/** @var resource*/
	protected $data;
	protected $name;
	protected $type;

	protected function __construct(){}

	public static function from_file(File $file)
	{
		if(!$file->is_file()) return false;

		$image = new Image();
		$size = getimagesize($file->get_path());
		$image->size = new Size($size[0], $size[1]);
		$image->type = $size['mime'];
		$image->create_image_by_type($file);

		return $image;
	}

	public static function from_size(Size $size)
	{
		$image = new Image();
		$image->size = $size;
		$image->create_image();
		$image->name = false;
		$image->type = false;

		return $image;
	}

	public static function from_url($url)
	{
		$image = new Image();
		$resource = fopen($url, 'r');
		$data = stream_get_contents($resource);
		fclose($resource);

		$metadata = getimagesizefromstring($data);

		$image->size = new Size($metadata[0], $metadata[1]);
		$image->type = $metadata['mime'];
		$image->name = basename($url);
		$image->data = imagecreatefromstring($data);

		return $image;
	}

	public function save(File $file)
	{
		switch ($this->type)
		{
			case 'image/png':
				$this->data = imagepng($this->data, $file->get_path(), 9);
				break;
			case 'image/jpeg':
				$this->data = imagejpeg($this->data, $file->get_path(), 80);
				break;
		}
	}

	public function save_as_jpeg(File $file, $quality = 80)
	{
		$this->data = imagejpeg($this->data, $file->get_path(), $quality);
	}

	public function output_as_jpeg($quality)
	{
		//header('Content-Disposition: Attachment;filename=image.jpg'); 
		header('Content-Type: image/jpeg');
		imagejpeg($this->data, NULL, $quality);
		imagedestroy($this->data);
		exit;
	}

	public function output_as_png()
	{
		//header('Content-Disposition: Attachment;filename=image.png'); 
		header('Content-Type: image/png');
		imagepng($this->data, NULL, 9);
		imagedestroy($this->data);
		exit;
	}

	public function scale($factor)
	{
		$this->data = imagescale($this->data, $this->size->get_width()*$factor, $this->size->get_height()*$factor);
	}

	public function get_resource()
	{
		return $this->data;
	}

	/**
	 * [get_resolution description]
	 * @return \backgrounder\objects\Size [description]
	 */
	public function get_size()
	{
		return $this->size;
	}


	protected function create_image_by_type(File $file)
	{
		switch ($this->type)
		{
			case 'image/png':
				$this->data = imagecreatefrompng($file->get_path());
				break;
			case 'image/jpeg':
				$this->data = imagecreatefromjpeg($file->get_path());
				break;
		}
	}

	protected function create_image()
	{
		$width = $this->size->get_width();
		$height = $this->size->get_height();
		$this->data = imagecreatetruecolor($width, $height);
	}
}