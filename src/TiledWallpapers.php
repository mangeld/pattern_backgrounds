<?php

namespace backgrounder;
use \backgrounder\objects\Image;
use \backgrounder\objects\Size;
use \http\HTTP_Requester;

class TiledWallpapers
{
	private $slim;

	public function __construct()
	{
		$this->slim = new \Slim\Slim();
		$this->init_routes();
	}

	public function run()
	{
		$this->slim->run();
	}

	public function parse_resolution($resolution)
	{
		//Check for invalid values
		if(!is_string($resolution)) return false;
		$resolution = strtolower($resolution);
		$res = explode('x', $resolution);
		if( !isset($res[0]) || !isset($res[1]) ) return false;
		if( !isset($res[2]) || $res[2] == "" ) $res[2] = 1;

		$res[0] = intval($res[0]);
		$res[1] = intval($res[1]);
		$res[2] = intval($res[2]);

		if( $res[0] == 0 || $res[1] == 0 || $res[2] == 0 ) return false;
		$res = array_slice($res, 0, 3);
		return $res;
	}

	public function create_background($pattern, Size $size, $scale = 3)
	{
		$tile = new \Imagick();
		$tile->readImage($pattern);
		$wallpaper = new \Imagick();
		$wallpaper->newImage($size->get_width(), $size->get_height(), new \ImagickPixel('black'));
		$wallpaper->setImageFormat('png');
		$res = $tile->getImageGeometry();
		$tile->resizeImage( $res['width']*$scale, $res['height']*$scale, \imagick::FILTER_LANCZOS, 1);
		$wallpaper = $wallpaper->textureImage($tile);
		$wallpaper->setImageFormat('png');
		return $wallpaper;
	}

	private function init_routes()
	{
		$this->slim->get('/', function(){
			$loader = new \Twig_Loader_Filesystem($_SERVER['DOCUMENT_ROOT'].'/website/templates/');
			$twig = new \Twig_Environment($loader, array());
			echo $twig->render('index.html');
		});

		$this->slim->get('/patterns/new', function(){
			$this->slim->response()->header('Content-Type', 'application/json');
			$requester = new \backgrounder\http\HTTP_Requester('http://www.colourlovers.com/api/patterns/new');
			$requester->set_variable('format', 'json');
			$requester->set_variable('numResults', 20);
			$data = $requester->execute();
			$this->slim->response()->setBody($data);
		});

		$this->slim->get('/wallpaper/:id(/:size)', function($id, $size = '1920x1080x2'){
			$requester = new \backgrounder\http\HTTP_Requester('http://www.colourlovers.com/api/pattern/'.$id);
			$requester->set_variable('format', 'json');
			$data = json_decode($requester->execute(), true);
			$data = $data[0];
			$s = $this->parse_resolution($size);
			$wallpaper = $this->create_background($data['imageUrl'], new \backgrounder\objects\Size($s[0], $s[1]), $s[2]);
			$this->slim->response->header('Content-type', $wallpaper->getImageFormat());
			$name = $id.'_'.$s[0].'_'.$s[1].'.png';
			$this->slim->response->header('Content-Disposition', 'attachment; filename="'.$name.'"');
			echo $wallpaper->getImageBlob();
		});
	}

	private function scale_image()
	{
		
	}
}