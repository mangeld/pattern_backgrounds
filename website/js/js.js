$(document).ready(function(){
	
	function handle_new_patterns(data)
	{
		var section = $('#tiles');

		for (var i = 0; i < data.length; i++)
		{
			var article = document.createElement("article");
			$(article).css('background-image', 'url("'+data[i].imageUrl+'")');
			$.data(article, "id", data[i].id); //Store the id for each pattern
			$(article).hide();

			$(article).ready(function(){
				$(article).show();
			});
			$(article).on('click', function(){
				clicked_article(this);
			});
			section.append(article);
		}
	}

	$.ajax({
		type: "GET",
		url: '/patterns/new',
		success : function(data){handle_new_patterns(data);}
	});

	/*$('#tiles article').on('click', function(){
		alert('que te pasa tronco');
		clicked_article(this);
	});*/

	function clicked_article (article)
	{
		var prev = $("#preview")
		var anim = {opacity : 1};
		var anim2 = {opacity : 0};
		var options = { 
			duration: 200,
			start : function(){ $('#preview').css('display', 'block'); } 
		};

		$('#preview article > div:nth-of-type(1)').css('background-image', $(article).css('background-image'));
		$('#preview article > div:nth-of-type(2) button').on('click', function(){
			window.location.href = 'wallpaper/'+$.data(article, 'id')+'/2560x1900x1';
		});

		$(prev).animate(anim, options);

		$(prev).on('click', function(event){
			if(event.target.id === 'preview')
			{
				$(this).animate(anim2, 200, function(){
					$(this).css('display', 'none');
				});
				
			}
		});
	}

});